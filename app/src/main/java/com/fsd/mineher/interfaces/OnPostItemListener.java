package com.fsd.mineher.interfaces;

import android.view.View;

public interface OnPostItemListener {
    void onUserNameClick(View v, int position);

    void onItemClick(View v, int position);
}
