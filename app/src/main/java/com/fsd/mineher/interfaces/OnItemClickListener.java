package com.fsd.mineher.interfaces;

public interface OnItemClickListener {
    void onItemClick(int position);
}
