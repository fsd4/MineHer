package com.fsd.mineher.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.fsd.mineher.model.Post;
import com.fsd.mineher.model.PostDao;
import com.fsd.mineher.model.PostDatabase;

import java.util.List;

public class PostRepo {
    private final PostDao postDao;

    public PostRepo(Application application) {
        PostDatabase database = PostDatabase.getInstance(application.getApplicationContext());
        postDao = database.postDao();
    }

    public void insert(Post post) {
        new Thread(() -> postDao.insertPost(post)).start();
    }

    public void update(Post post) {
        new Thread(() -> postDao.updatePost(post)).start();
    }

    public void delete(Post post) {
        new Thread(() -> postDao.deletePost(post)).start();
    }
    public void deleteAllPosts(){
        new Thread(postDao::deleteAllPosts).start();
    }

    public LiveData<List<Post>> getAllPosts() {
        return postDao.getAllPosts();
    }

    public LiveData<List<Post>> getAllPostsById(String userId) {
        return postDao.getAllPostsById(userId);
    }

}
