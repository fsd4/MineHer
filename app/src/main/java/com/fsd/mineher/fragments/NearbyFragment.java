package com.fsd.mineher.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.fsd.mineher.R;
import com.fsd.mineher.activities.OtherUserActivity;
import com.fsd.mineher.model.ModelFirebase;
import com.fsd.mineher.model.User;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class NearbyFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap map;
    private List<User> users;
    private int lastIndex = -1;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_nearby, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(31.0461, 34.8516), 8));
        ModelFirebase.getAllUsers(data -> {
            users = data;
            for (int i = 0; i < users.size(); i++) {
                Marker marker = map.addMarker(new MarkerOptions().position(new LatLng(users.get(i).getLat(), users.get(i).getLon())).title(users.get(i).getUserUsername()));
                marker.setTag(i);
                map.setOnMarkerClickListener(markerClicked -> {
                    if (lastIndex == Integer.parseInt(markerClicked.getTag().toString())) {
                        if (!User.getInstance().getUserId().equals(users.get((int) markerClicked.getTag()).getUserId())){
                            Intent intent = new Intent(getContext(), OtherUserActivity.class);
                            intent.putExtra("otherUser", users.get((int) markerClicked.getTag()));
                            startActivity(intent);
                        }
                        lastIndex = -1;
                    } else {
                        lastIndex = Integer.parseInt(markerClicked.getTag().toString());
                        markerClicked.showInfoWindow();
                    }
                    return true;
                });
            }
        });
    }
}
