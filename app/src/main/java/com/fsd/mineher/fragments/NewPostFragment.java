package com.fsd.mineher.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.fsd.mineher.R;
import com.fsd.mineher.model.Post;
import com.fsd.mineher.model.StoreModel;
import com.fsd.mineher.model.User;
import com.fsd.mineher.viewmodels.PostViewModel;
import com.google.android.material.snackbar.Snackbar;

import java.io.FileDescriptor;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;

public class NewPostFragment extends Fragment {


    private PostViewModel postViewModel;
    private View view;
    private ProgressBar progressBar;
    private EditText postContentInput;
    private EditText contactInput;
    private ImageView postImageView;
    private Uri postImgUri;
    private Bitmap postImgBitmap;
    private static int REQUEST_CODE = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_new_post, container, false);

        postViewModel = new ViewModelProvider(this).get(PostViewModel.class);

        progressBar = view.findViewById(R.id.new_post_fragment_progress_bar);
        progressBar.setVisibility(View.INVISIBLE);
        postImageView = view.findViewById(R.id.new_post_fragment_image_view);
        postContentInput = view.findViewById(R.id.new_post_fragment_content_edit_text);
        contactInput = view.findViewById(R.id.new_post_fragment_contact_edit_text);

        postImageView.setOnClickListener(view -> chooseImageFromGallery());

        Button publishBtn = view.findViewById(R.id.new_post_fragment_publish_btn);
        publishBtn.setOnClickListener(v -> {
            if (postImgUri != null && postContentInput != null && contactInput != null)
                savePost();
            else
                Toast.makeText(getContext(), "Please fill all fields and add a photo", Toast.LENGTH_SHORT).show();
        });

        return view;
    }

    void savePost() {
        progressBar.setVisibility(View.VISIBLE);
        final Post newPost = generateNewPost();
        StoreModel.uploadImage(postImgBitmap, new StoreModel.Listener() {
            @Override
            public void onSuccess(String url) {
                newPost.setPostImgUrl(url);
                postViewModel.insert(newPost, data -> {
                    NavController navCtrl = Navigation.findNavController(view);
                    navCtrl.navigateUp();
                });
            }

            @Override
            public void onFail() {
                progressBar.setVisibility(View.INVISIBLE);
                Snackbar.make(view, "Failed to create post and save it in databases", Snackbar.LENGTH_LONG).show();
            }
        });
    }

    void chooseImageFromGallery() {
        try {
            Intent openGalleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
            openGalleryIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");

            startActivityForResult(openGalleryIntent, REQUEST_CODE);
        } catch (Exception e) {
            Toast.makeText(getActivity(), "New post Page: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null) return;
        if (data.getData() != null && resultCode == RESULT_OK) {
            postImgUri = data.getData();
            postImageView.setImageURI(postImgUri);
            postImgBitmap = uriToBitmap(postImgUri);
        } else {
            Toast.makeText(getActivity(), "No image was selected", Toast.LENGTH_SHORT).show();
        }
    }

    private Post generateNewPost() {
        Post newPost = new Post();
        newPost.setUserId(User.getInstance().getUserId());
        newPost.setUserProfileImageUrl(User.getInstance().getProfileImageUrl());
        newPost.setUsername(User.getInstance().getUserUsername());
        newPost.setContact(contactInput.getText().toString());
        newPost.setPostContent(postContentInput.getText().toString());
        return newPost;
    }

    private Bitmap uriToBitmap(Uri selectedFileUri) {
        try {
            ParcelFileDescriptor parcelFileDescriptor = getContext().getContentResolver().openFileDescriptor(selectedFileUri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
            parcelFileDescriptor.close();
            return image;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}