package com.fsd.mineher.fragments;

import androidx.annotation.NonNull;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;

import com.fsd.mineher.R;

public class ProfileFragmentDirections {
    private ProfileFragmentDirections() {
    }

    @NonNull
    public static NavDirections actionProfileFragmentToEditProfileActivity() {
        return new ActionOnlyNavDirections(R.id.action_profileFragment_to_editProfileActivity);
    }

    @NonNull
    public static NavDirections actionProfileFragmentToPostsActivity() {
        return new ActionOnlyNavDirections(R.id.action_profileFragment_to_postsActivity);
    }
}
