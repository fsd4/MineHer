package com.fsd.mineher.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.fsd.mineher.R;
import com.fsd.mineher.model.User;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment {

    private TextView userUsername;
    private TextView userEmail;
    private TextView userInfo;
    private CircleImageView userProfileImage;
    private ImageButton editProfileBtn;
    private Button profilePosts;

    @Override
    public void onResume() {
        super.onResume();
        setUserProfile();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        userUsername = view.findViewById(R.id.profile_fragment_username_text_view);
        userEmail = view.findViewById(R.id.profile_fragment_email_text_view);
        userInfo = view.findViewById(R.id.profile_info_text);
        userProfileImage = view.findViewById(R.id.profile_fragment_profile_image_view);

        editProfileBtn = view.findViewById(R.id.profile_fragment_edit_profile_btn);
        editProfileBtn.setOnClickListener(view1 -> toEditProfilePage());
        profilePosts = view.findViewById(R.id.profile_posts_btn);
        profilePosts.setOnClickListener(view1 -> toPostsPage());

        return view;
    }

    private void toEditProfilePage() {
        NavController navCtrl = Navigation.findNavController(getActivity(), R.id.main_nav_host);
        NavDirections directions = ProfileFragmentDirections.actionProfileFragmentToEditProfileActivity();
        navCtrl.navigate(directions);
    }

    private void toPostsPage() {
        NavController navCtrl = Navigation.findNavController(getActivity(), R.id.main_nav_host);
        NavDirections directions = ProfileFragmentDirections.actionProfileFragmentToPostsActivity();
        navCtrl.navigate(directions);
    }

    public void setUserProfile() {
        userUsername.setText(User.getInstance().getUserUsername());
        userEmail.setText(User.getInstance().getUserEmail());
        userInfo.setText(User.getInstance().getUserInfo());

        if (!User.getInstance().getProfileImageUrl().equals(""))
            Picasso.get().load(User.getInstance().getProfileImageUrl()).noPlaceholder().into(userProfileImage);
    }

}