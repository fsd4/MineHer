package com.fsd.mineher.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.fsd.mineher.R;
import com.fsd.mineher.adapter.FeedViewPagerAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class FeedsFragment extends Fragment {
    private ViewPager2 mviewPager;
    private FeedViewPagerAdapter feedAdapter;
    private TabLayout feedTabLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feeds, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mviewPager = view.findViewById(R.id.main_view_pager);
        feedTabLayout = view.findViewById(R.id.main_tab_layout);
        feedAdapter = new FeedViewPagerAdapter(this);
        mviewPager.setOffscreenPageLimit(feedAdapter.getItemCount());
        mviewPager.setAdapter(feedAdapter);
        mviewPager.setUserInputEnabled(false);
        String[] titles = {"Following", "Nearby"};
        new TabLayoutMediator(feedTabLayout, mviewPager, (tab, position) -> tab.setText(titles[position])).attach();
    }
}