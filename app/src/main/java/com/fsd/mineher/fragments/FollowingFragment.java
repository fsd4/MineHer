package com.fsd.mineher.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.fsd.mineher.R;
import com.fsd.mineher.activities.OtherUserActivity;
import com.fsd.mineher.activities.PostDetailsActivity;
import com.fsd.mineher.adapter.PostRecyclerAdapter;
import com.fsd.mineher.interfaces.OnPostItemListener;
import com.fsd.mineher.model.ModelFirebase;
import com.fsd.mineher.model.Post;
import com.fsd.mineher.model.User;
import com.fsd.mineher.viewmodels.PostViewModel;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class FollowingFragment extends Fragment {
    private PostViewModel postViewModel;
    private RecyclerView followingRecycler;
    private PostRecyclerAdapter adapter;
    private Button followUsersBtn;
    List<Post> filteredPosts = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_following, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        followUsersBtn = view.findViewById(R.id.following_new_users_btn);
        followUsersBtn.setOnClickListener(view1 -> {
            NavController navCtrl = Navigation.findNavController(getActivity(), R.id.main_nav_host);
            navCtrl.navigate(new ActionOnlyNavDirections(R.id.action_feedListFragment_to_findUsersActivity));
        });
        followingRecycler = view.findViewById(R.id.following_recycler_view);
        followingRecycler.setHasFixedSize(true);
        adapter = new PostRecyclerAdapter();
        followingRecycler.setAdapter(adapter);
        postViewModel = new ViewModelProvider(this).get(PostViewModel.class);

        adapter.setOnPostClickListener(new OnPostItemListener() {
            @Override
            public void onUserNameClick(View v, int position) {
                ModelFirebase.getUser(filteredPosts.get(position).getUserId(), data -> {
                    Intent intent = new Intent(getContext(), OtherUserActivity.class);
                    intent.putExtra("otherUser", data);
                    startActivity(intent);
                });
            }

            @Override
            public void onItemClick(View v, int position) {
                Intent intent = new Intent(getContext(), PostDetailsActivity.class);
                intent.putExtra("post", new Gson().toJson(filteredPosts.get(position)));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        postViewModel.deleteAllPosts();
    }

    @Override
    public void onResume() {
        super.onResume();

        ModelFirebase.getAllPostsFollowing(data -> {
            for (Post post : data)
                postViewModel.insertLocaly(post);
        });
        postViewModel.getAllPosts().observe(getViewLifecycleOwner(), posts -> {
            filteredPosts = new ArrayList<>();
            for (Post post : posts)
                if (User.getInstance().getFollowingUsers().contains(post.getUserId()) && post.isAvailable())
                    filteredPosts.add(post);
            filteredPosts.sort((o1, o2) -> (int) (o2.getLastUpdated().getSeconds() - o1.getLastUpdated().getSeconds()));
            adapter.setPosts(filteredPosts);
        });
    }
}