package com.fsd.mineher;
import android.app.Application;
import android.content.Context;

public class MineHerApplication extends Application{
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }
    public static Context getAppContext(){
        return context;
    }
}
