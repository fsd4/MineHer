package com.fsd.mineher.converters;

import androidx.room.TypeConverter;

import com.google.firebase.Timestamp;

import java.util.Date;

public class Converters {
    @TypeConverter
    public static Timestamp fromTimestamp(Long value) {
        return value == null ? null : new Timestamp(new Date(value));
    }

    @TypeConverter
    public static Long dateToTimestamp(Timestamp timestamp) {
        return timestamp == null ? null : timestamp.getSeconds();
    }
}