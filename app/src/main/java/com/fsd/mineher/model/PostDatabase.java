package com.fsd.mineher.model;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.fsd.mineher.converters.Converters;

@Database(entities = {Post.class}, version = 6)
@TypeConverters({Converters.class})
public abstract class PostDatabase extends RoomDatabase {
    private static PostDatabase instance;

    public abstract PostDao postDao();

    public static synchronized PostDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    PostDatabase.class, "post_database")
                    .fallbackToDestructiveMigration().build();
        }
        return instance;
    }
}

