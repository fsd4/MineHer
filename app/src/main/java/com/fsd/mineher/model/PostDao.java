package com.fsd.mineher.model;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface PostDao {

    @Query("select * from Post")
    LiveData<List<Post>> getAllPosts();

    @Query("select * from Post where userId= :userId")
    LiveData<List<Post>> getAllPostsById(String userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPost(Post post);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllPosts(Post... posts);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updatePost(Post post);

    @Delete
    void deletePost(Post post);

    @Query("delete from Post")
    void deleteAllPosts();

    @Query("select exists(select * from Post where postId = :postId)")
    boolean isPostExists(String postId);

    @Query("delete from Post where postId = :postId")
    void deleteByPostId(String postId);
}
