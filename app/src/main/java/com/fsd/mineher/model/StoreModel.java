package com.fsd.mineher.model;

import android.graphics.Bitmap;
import android.net.Uri;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.Date;

public class StoreModel {
    static FirebaseStorage storage = FirebaseStorage.getInstance();

    public interface Listener {
        void onSuccess(String url);

        void onFail();
    }

    public static void uploadImage(Bitmap imageBitmap, final Listener listener) {

        Date date = new Date();
        String imageName = User.getInstance().getUserUsername() + date.getTime();

        final StorageReference imageRef = storage.getReference().child("images").child(imageName);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        UploadTask uploadTask = imageRef.putBytes(data);
        uploadTask.addOnFailureListener(e -> listener.onFail()).addOnSuccessListener(taskSnapshot -> imageRef.getDownloadUrl().addOnSuccessListener(uri -> {
            Uri downloadUrl = uri;
            listener.onSuccess(downloadUrl.toString());
        }));
    }

    public static void deleteImage(String imageUrl, final Listener listener) {

        final StorageReference imageRef = storage.getReferenceFromUrl(imageUrl);
        imageRef.delete().addOnSuccessListener(aVoid -> listener.onSuccess("")).addOnFailureListener(e -> listener.onFail());
    }
}
