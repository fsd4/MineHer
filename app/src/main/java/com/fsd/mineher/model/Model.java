package com.fsd.mineher.model;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.List;

import static com.fsd.mineher.MineHerApplication.getAppContext;


public class Model {

    public static final Model instance = new Model();

    public interface Listener<T> {
        void onComplete(T data);
    }

    public interface CompListener {
        void onComplete();
    }

    private Model() {
    }


}
