package com.fsd.mineher.model;

import android.content.ContentResolver;
import android.net.Uri;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.fsd.mineher.MineHerApplication.getAppContext;

public class ModelFirebase {
    final static String POSTS_COLLECTION = "posts";
    final static String USERS_COLLECTION = "users";
    final static FirebaseAuth auth = FirebaseAuth.getInstance();
    final static FirebaseFirestore firestore = FirebaseFirestore.getInstance();

    public static void signOut() {
        auth.signOut();
    }

    public static void updateUser(String followingUsers, List<String> followingUsersList) {
        firestore.collection(USERS_COLLECTION).document(User.getInstance().getUserId()).update(followingUsers, followingUsersList);
    }

    public static void updateUser(User user, Model.Listener<Boolean> listener) {
        firestore.collection(USERS_COLLECTION).document(user.getUserId()).set(user).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                listener.onComplete(task.isSuccessful());
            }
        });
    }

    public static void getAllOtherUsers(Model.Listener<List<User>> listener){
        FirebaseFirestore.getInstance().collection("users").whereNotEqualTo("userId", User.getInstance().getUserId()).get().addOnSuccessListener(queryDocumentSnapshots -> {
            listener.onComplete(queryDocumentSnapshots.toObjects(User.class));
        });
    }

    public static void getAllPostsFollowing(Model.Listener<List<Post>> listListener) {
        firestore.collection(POSTS_COLLECTION).get().addOnSuccessListener(queryDocumentSnapshots -> {
            if (listListener != null)
                listListener.onComplete(queryDocumentSnapshots.toObjects(Post.class));
        });
    }

    public static void updatePost(Post post, Model.Listener<Boolean> listener) {
        firestore.collection(POSTS_COLLECTION).document(post.getPostId()).set(post).addOnCompleteListener(task -> listener.onComplete(task.isSuccessful()));
    }

    public static void removePost(Post post, Model.Listener<Boolean> listener) {
        firestore.collection(POSTS_COLLECTION).document(post.getPostId()).delete().addOnCompleteListener(task -> listener.onComplete(task.isSuccessful()));
    }

    public static void getUser(String userId, Model.Listener<User> userListener) {
        firestore.collection(USERS_COLLECTION).document(userId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                userListener.onComplete(documentSnapshot.toObject(User.class));
            }
        });
    }

    public static void getAllUsers(Model.Listener<List<User>> userListener) {
        firestore.collection(USERS_COLLECTION).get().addOnSuccessListener(queryDocumentSnapshots -> {
            userListener.onComplete(queryDocumentSnapshots.toObjects(User.class));
        });
    }

    public static void getAllPostsById(String userId, Model.Listener<List<Post>> listener) {
        FirebaseFirestore.getInstance().collection("posts").whereEqualTo("userId", userId).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                listener.onComplete(queryDocumentSnapshots.toObjects(Post.class));
            }
        });
    }

    public interface Listener<T> {
        void onComplete();

        void onFail();
    }


    public static void loginUser(final String email, String password, final Listener<Boolean> listener) {

        if (email != null && !email.equals("") && password != null && !password.equals("")) {
            auth.signInWithEmailAndPassword(email, password).addOnSuccessListener(authResult -> {
                getUserData();
                listener.onComplete();
            }).addOnFailureListener(e -> {
                Toast.makeText(getAppContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                listener.onFail();
            });
        } else {
            Toast.makeText(getAppContext(), "Please fill both data fields", Toast.LENGTH_SHORT).show();
        }
    }

    public static void registerUserAccount(final String username, String password, final String email, final Uri imageUri, final double lat, final double lon, final Listener<Boolean> listener) {

        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Toast.makeText(getAppContext(), "User registered", Toast.LENGTH_SHORT).show();
                uploadUserData(username, email, imageUri, listener, lat, lon);

            } else {
                Toast.makeText(getAppContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                listener.onFail();
            }
        });

    }


    private static void uploadUserData(final String username, final String email, Uri imageUri, final Listener<Boolean> listener, final double lat, final double lon) {


        StorageReference storageReference = FirebaseStorage.getInstance().getReference("images");

        if (imageUri != null) {
            String imageName = username + "." + getExtension(imageUri);
            final StorageReference imageRef = storageReference.child(imageName);

            UploadTask uploadTask = imageRef.putFile(imageUri);
            uploadTask.continueWithTask(task -> {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                return imageRef.getDownloadUrl();
            }).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    User user = User.getInstance();
                    user.setUserUsername(username);
                    user.setUserEmail(email);
                    user.setProfileImageUrl(task.getResult().toString());
                    user.setUserUsername(username);
                    user.setUserId(auth.getUid());
                    user.setLat(lat);
                    user.setLon(lon);

                    firestore.collection(USERS_COLLECTION).document(user.getUserId()).set(user).addOnSuccessListener(aVoid -> {
                        Toast.makeText(getAppContext(), "User created successfully", Toast.LENGTH_SHORT).show();
                        listener.onComplete();
                    }).addOnFailureListener(e -> Toast.makeText(getAppContext(), "Fails to create user and upload data: " + e.getMessage(), Toast.LENGTH_SHORT).show());
                } else if (!task.isSuccessful()) {
                    Toast.makeText(getAppContext(), task.getException().toString(), Toast.LENGTH_SHORT).show();
                    listener.onFail();
                }
            });
        } else {
            Toast.makeText(getAppContext(), "Please choose a profile image", Toast.LENGTH_SHORT).show();
            listener.onFail();
        }
    }

    public static String getExtension(Uri uri) {
        try {
            ContentResolver contentResolver = getAppContext().getContentResolver();
            MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));

        } catch (Exception e) {
            Toast.makeText(getAppContext(), "Register page: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            return null;
        }
    }


    //----------------------------------------------------------------------------------------

    public static void getAllPostsSince(long since, final Model.Listener<List<Post>> listener) {

        Timestamp ts = new Timestamp(since, 0);
        firestore.collection(POSTS_COLLECTION).whereGreaterThanOrEqualTo("lastUpdated", ts).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                List<Post> postsData = null;
                if (task.isSuccessful()) {
                    postsData = new LinkedList<Post>();
                    for (QueryDocumentSnapshot doc : task.getResult()) {
                        Map<String, Object> json = doc.getData();
                        Post posts = factory(json);
                        postsData.add(posts);
                    }
                }
                listener.onComplete(postsData);
                Log.d("TAG", "refresh " + postsData.size());
            }
        });
    }

    public static void addPost(Post post, final Model.Listener<Boolean> listener) {
        DocumentReference dr = firestore.collection(POSTS_COLLECTION).document();
        post.setPostId(dr.getId());
        dr.set(post).addOnCompleteListener(task -> {
            if (listener != null) {
                listener.onComplete(task.isSuccessful());
            }
            if (task.isSuccessful())
                Toast.makeText(getAppContext(), "Post uploaded successfully", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getAppContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
        });
    }

    public static void deletePost(final Post post, final Model.Listener<Boolean> listener) {
        firestore.collection(POSTS_COLLECTION).document(post.getPostId()).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Map<String, Object> deleted = new HashMap<>();
                deleted.put("postId", post.getPostId());
                firestore.collection("deleted").document(post.getPostId()).set(deleted).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (listener != null) {
                            listener.onComplete(task.isSuccessful());
                        }
                    }
                });
            }
        });
    }

    public static void getDeletedPostsId(final Model.Listener<List<String>> listener) {
        firestore.collection("deleted").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                List<String> deletedPostsIds = null;
                if (task.isSuccessful()) {
                    deletedPostsIds = new LinkedList<String>();
                    for (QueryDocumentSnapshot doc : task.getResult()) {
                        String deleted = (String) doc.getData().get("postId");
                        deletedPostsIds.add(deleted);
                    }
                }
                listener.onComplete(deletedPostsIds);
            }
        });
    }

    private static Post factory(Map<String, Object> json) {
        Post newPost = new Post();
        newPost.setPostId(json.get("postId").toString());
        newPost.setPostContent(json.get("postContent").toString());
        newPost.setPostImgUrl(json.get("postImgUrl").toString());
        newPost.setUserId(json.get("userId").toString());
        newPost.setUserProfileImageUrl(json.get("userProfilePicUrl").toString());
        newPost.setUsername(json.get("username").toString());
        newPost.setContact(json.get("contact").toString());
        Timestamp ts = (Timestamp) json.get("lastUpdated");
        if (ts != null)
            newPost.setLastUpdated(ts);
        return newPost;
    }

    private static Map<String, Object> toJson(Post post) {
        HashMap<String, Object> json = new HashMap<>();
        json.put("postId", post.getPostId());
        json.put("postContent", post.getPostContent());
        json.put("postImgUrl", post.getPostImgUrl());
        json.put("userId", post.getUserId());
        json.put("userProfilePicUrl", post.getUserProfileImageUrl());
        json.put("username", post.getUsername());
        json.put("contact", post.getContact());
        json.put("lastUpdated", FieldValue.serverTimestamp());
        return json;
    }

    public static void updateUserProfile(String username, String info, String profileImgUrl, final Model.Listener<Boolean> listener) {

        if (!username.equals(""))
            User.getInstance().setUserUsername(username);
        if (!info.equals(""))
            User.getInstance().setUserInfo(info);
        if (!profileImgUrl.equals(""))
            User.getInstance().setProfileImageUrl(profileImgUrl);

        firestore.collection(USERS_COLLECTION).document(User.getInstance().getUserId()).set(User.getInstance()).addOnCompleteListener(task -> {
            if (listener != null)
                listener.onComplete(task.isSuccessful());
        });
    }

    public static void getUserData() {
        if (FirebaseAuth.getInstance().getCurrentUser() == null) return;
        firestore.collection(USERS_COLLECTION).document(auth.getCurrentUser().getUid()).get().addOnSuccessListener(documentSnapshot -> {
            User.getInstance().setUser(documentSnapshot.toObject(User.class));
        }).addOnFailureListener(e -> {
            Toast.makeText(getAppContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        });
    }


}
