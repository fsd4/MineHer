package com.fsd.mineher.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//singleton user class
public class User implements Serializable {

    private static User theUser = null;

    private String userUsername;
    private String userEmail;
    private String profileImageUrl;
    private String userId;
    private String userInfo;
    private List<String> followingUsers;
    private double lat;
    private double lon;


    public void setUser(User user) {
        this.userUsername = user.userUsername;
        this.userEmail = user.userEmail;
        this.profileImageUrl = user.profileImageUrl;
        this.userId = user.userId;
        this.userInfo = user.userInfo;
        this.followingUsers = user.followingUsers;
        this.lat = user.getLat();
        this.lon = user.getLon();
    }


    public List<String> getFollowingUsers() {
        return followingUsers;
    }

    public void setFollowingUsers(List<String> followingUsers) {
        this.followingUsers = followingUsers;
    }

    public String getUserUsername() {
        return userUsername;
    }

    public void setUserUsername(String userUsername) {
        this.userUsername = userUsername;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(String userInfo) {
        this.userInfo = userInfo;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public User() {
        userEmail = "";
        userUsername = "";
        profileImageUrl = "";
        userId = "";
        userInfo = "";
        followingUsers = new ArrayList<>();
        lat = 31.0461;
        lon = 34.8516;
    }

    // static method to create instance of Singleton class
    public static User getInstance() {
        if (theUser == null)
            theUser = new User();
        return theUser;
    }

}
