package com.fsd.mineher.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.firebase.Timestamp;

import java.io.Serializable;

@Entity
public class Post implements Serializable {
    @PrimaryKey
    @NonNull
    private String postId;
    private String postContent;
    private String postImgUrl;
    private String userId;
    private String userProfileImageUrl;
    private String username;
    private String contact;
    private boolean isAvailable;
    private Timestamp lastUpdated;


    public Post() {
        postId = "";
        postContent = "";
        postImgUrl = "";
        userId = "";
        userProfileImageUrl = "";
        username = "";
        contact = "";
        isAvailable = true;
        lastUpdated = Timestamp.now();
    }

    public Post(String postId, String postContent, String postImgUrl, String userId, String userProfileImageUrl, String username, String contact, boolean isAvailable, Timestamp lastUpdated, double lat, double lon) {
        this.postId = postId;
        this.postContent = postContent;
        this.postImgUrl = postImgUrl;
        this.userId = userId;
        this.userProfileImageUrl = userProfileImageUrl;
        this.username = username;
        this.contact = contact;
        this.isAvailable = isAvailable;
        this.lastUpdated = lastUpdated;

    }

    public String getUserProfileImageUrl() {
        return userProfileImageUrl;
    }

    public void setUserProfileImageUrl(String userProfileImageUrl) {
        this.userProfileImageUrl = userProfileImageUrl;
    }

    @NonNull
    public String getPostId() {
        return postId;
    }

    public void setPostId(@NonNull String postId) {
        this.postId = postId;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public String getPostImgUrl() {
        return postImgUrl;
    }

    public void setPostImgUrl(String postImgUrl) {
        this.postImgUrl = postImgUrl;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public Timestamp getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Timestamp lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

}
