package com.fsd.mineher.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.fsd.mineher.R;
import com.fsd.mineher.adapter.PostRecyclerAdapter;
import com.fsd.mineher.interfaces.OnPostItemListener;
import com.fsd.mineher.model.ModelFirebase;
import com.fsd.mineher.model.User;
import com.fsd.mineher.viewmodels.PostViewModel;
import com.google.gson.Gson;

public class PostsActivity extends AppCompatActivity {

    private User user;
    private PostViewModel postViewModel;
    private RecyclerView postRecycler;
    private PostRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);
        postViewModel = new ViewModelProvider(this).get(PostViewModel.class);
        postRecycler = findViewById(R.id.user_recycler_view);
        postRecycler.setHasFixedSize(true);
        adapter = new PostRecyclerAdapter();
        postRecycler.setAdapter(adapter);
        user = (User) getIntent().getExtras().get("user");
        if (user == null) {
            user = User.getInstance();
            postViewModel.getAllPostsById(user.getUserId()).observe(this, posts -> adapter.setPosts(posts));
            adapter.setOnPostClickListener(new OnPostItemListener() {
                @Override
                public void onUserNameClick(View v, int position) {

                }

                @Override
                public void onItemClick(View v, int position) {
                    Intent intent = new Intent(getApplicationContext(), EditPostActivity.class);
                    intent.putExtra("post", new Gson().toJson(adapter.getPosts().get(position)));
                    startActivity(intent);
                }
            });
        } else {
            ModelFirebase.getAllPostsById(user.getUserId(), data -> {
                adapter.setPosts(data);
            });

            adapter.setOnPostClickListener(new OnPostItemListener() {
                @Override
                public void onUserNameClick(View v, int position) {
                }

                @Override
                public void onItemClick(View v, int position) {
                    Intent intent = new Intent(getApplicationContext(), PostDetailsActivity.class);
                    intent.putExtra("post", new Gson().toJson(adapter.getPosts().get(position)));
                    startActivity(intent);
                }
            });
        }
    }
}