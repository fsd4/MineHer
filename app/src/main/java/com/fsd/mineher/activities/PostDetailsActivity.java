package com.fsd.mineher.activities;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.fsd.mineher.R;
import com.fsd.mineher.model.Post;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class PostDetailsActivity extends AppCompatActivity {

    private Post post;
    private CircleImageView profileImg;
    private TextView usernameText;
    private ImageView postImg;
    private TextView contentText;
    private TextView contactText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_details);
        post = new Gson().fromJson(getIntent().getExtras().getString("post"), Post.class);
        profileImg = findViewById(R.id.post_details_profile_img);
        usernameText = findViewById(R.id.post_details_username);
        contentText = findViewById(R.id.post_details_content);
        contactText = findViewById(R.id.post_details_contact);
        postImg = findViewById(R.id.post_details_img);
        usernameText.setText(post.getUsername());
        contentText.setText(post.getPostContent());
        contactText.setText(post.getContact());
        Picasso.get().load(post.getUserProfileImageUrl()).noPlaceholder().into(profileImg);
        Picasso.get().load(post.getPostImgUrl()).noPlaceholder().into(postImg);


    }
}