package com.fsd.mineher.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.fsd.mineher.R;
import com.fsd.mineher.model.ModelFirebase;
import com.fsd.mineher.model.StoreModel;
import com.fsd.mineher.model.User;
import com.squareup.picasso.Picasso;

import java.io.FileDescriptor;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends AppCompatActivity {

    CircleImageView profilePicImageView;
    EditText usernameInput;
    EditText infoInput;
    ProgressBar progressBar;
    Uri profileImageUrl;
    Bitmap postImgBitmap;
    static int REQUEST_CODE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        usernameInput = findViewById(R.id.edit_profile_activity_username_edit_text);
        infoInput = findViewById(R.id.edit_profile_activity_info_edit_text);
        profilePicImageView = findViewById(R.id.edit_profile_activity_profile_image_view);
        progressBar = findViewById(R.id.edit_profile_activity_progress_bar);
        progressBar.setVisibility(View.INVISIBLE);

        setEditProfileHints();
    }

    private void setEditProfileHints() {
        if (!User.getInstance().getProfileImageUrl().equals("")) {
            Picasso.get().load(User.getInstance().getProfileImageUrl()).noPlaceholder().into(profilePicImageView);
        }
        usernameInput.setHint(User.getInstance().getUserUsername());
        infoInput.setHint(User.getInstance().getUserInfo());
    }

    public void updateUserProfile(View view) {
        final String username;
        final String info;
        progressBar.setVisibility(View.VISIBLE);
        if (usernameInput.getText().toString() != null && !usernameInput.getText().toString().equals(""))
            User.getInstance().setUserUsername(usernameInput.getText().toString());
        if (infoInput.getText().toString() != null && !infoInput.getText().toString().equals(""))
            User.getInstance().setUserInfo(infoInput.getText().toString());

        if (profileImageUrl != null) {
            StoreModel.uploadImage(postImgBitmap, new StoreModel.Listener() {
                @Override
                public void onSuccess(String url) {
                    User.getInstance().setProfileImageUrl(url);
                    ModelFirebase.updateUser(User.getInstance(), data -> {
                        Toast.makeText(getApplicationContext(), "User Updated", Toast.LENGTH_SHORT).show();
                        finish();
                    });
                }

                @Override
                public void onFail() {

                }
            });
        } else {
            ModelFirebase.updateUser(User.getInstance(), data -> {
                Toast.makeText(getApplicationContext(), "User Updated", Toast.LENGTH_SHORT).show();
                finish();
            });
        }

    }

    public void chooseImageFromGallery(View view) {

        try {
            Intent openGalleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
            openGalleryIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");

            startActivityForResult(openGalleryIntent, REQUEST_CODE);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Edit profile Page: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            if (data.getData() != null) {
                profileImageUrl = data.getData();
                profilePicImageView.setImageURI(profileImageUrl);
                postImgBitmap = uriToBitmap(profileImageUrl);
            }
        } else {
            Toast.makeText(getApplicationContext(), "No image was selected", Toast.LENGTH_SHORT).show();
        }
    }

    private Bitmap uriToBitmap(Uri selectedFileUri) {
        try {
            ParcelFileDescriptor parcelFileDescriptor = getApplicationContext().getContentResolver().openFileDescriptor(selectedFileUri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
            parcelFileDescriptor.close();
            return image;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void closeEditProfile(View view) {
        finish();
    }
}