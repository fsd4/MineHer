package com.fsd.mineher.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.fsd.mineher.R;
import com.fsd.mineher.Utils;
import com.fsd.mineher.model.ModelFirebase;

import static com.fsd.mineher.MineHerApplication.getAppContext;

public class RegisterActivity extends AppCompatActivity implements LocationListener {

    private EditText usernameInput;
    private EditText passwordInput;
    private EditText emailInput;
    private Button registerBtn;
    private ImageView profileImageView;
    private ProgressBar progressBar;
    private Uri profileImageUri = null;
    private double lat = 31.0461;
    private double lon = 34.8516;
    private LocationManager locationManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_page);

        usernameInput = findViewById(R.id.register_activity_username_edit_text);
        passwordInput = findViewById(R.id.register_activity_password_edit_text);
        emailInput = findViewById(R.id.register_activity_email_edit_text);

        profileImageView = findViewById(R.id.register_add_img_icon_activity_imageView);


        profileImageView.setOnClickListener(view -> {
            Utils.chooseImageFromGallery(RegisterActivity.this);
            findViewById(R.id.register_add_img_activity_text_view).setVisibility(View.INVISIBLE);
        });

        progressBar = findViewById(R.id.register_activity_progress_bar);
        progressBar.setVisibility(View.INVISIBLE);

        registerBtn = findViewById(R.id.register_activity_register_btn);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        checkForLocation();

        registerBtn.setOnClickListener(view -> {

            if (usernameInput.getText().toString().isEmpty())
                Toast.makeText(getAppContext(), "You must enter username", Toast.LENGTH_SHORT).show();
            else if (passwordInput.getText().toString().isEmpty())
                Toast.makeText(getAppContext(), "You must enter password", Toast.LENGTH_SHORT).show();
            else if (emailInput.getText().toString().isEmpty())
                Toast.makeText(getAppContext(), "You must enter email", Toast.LENGTH_SHORT).show();
            else if (profileImageUri == null)
                Toast.makeText(getAppContext(), "You must enter image", Toast.LENGTH_SHORT).show();
            else {
                progressBar.setVisibility(View.VISIBLE);
                ModelFirebase.registerUserAccount(usernameInput.getText().toString(), passwordInput.getText().toString(), emailInput.getText().toString(), profileImageUri, lat, lon, new ModelFirebase.Listener<Boolean>() {
                    @Override
                    public void onComplete() {

                        progressBar.setVisibility(View.INVISIBLE);
                        ModelFirebase.loginUser(emailInput.getText().toString(), passwordInput.getText().toString(), new ModelFirebase.Listener<Boolean>() {
                            @Override
                            public void onComplete() {
                                finishAffinity();
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            }

                            @Override
                            public void onFail() {


                            }
                        });
                    }

                    @Override
                    public void onFail() {
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });
            }
        });
    }

    private void checkForLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (data.getData() != null) {
                profileImageUri = data.getData();
                profileImageView.setImageURI(profileImageUri);
            } else {
                Toast.makeText(this, "No image was selected", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void closeRegister(View view) {
        finish();
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        lat = location.getLatitude();
        lon = location.getLongitude();
    }
}