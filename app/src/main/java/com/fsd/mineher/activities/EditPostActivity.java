package com.fsd.mineher.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import com.fsd.mineher.R;
import com.fsd.mineher.model.Post;
import com.fsd.mineher.model.StoreModel;
import com.fsd.mineher.viewmodels.PostViewModel;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.Timestamp;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.FileDescriptor;
import java.io.IOException;

public class EditPostActivity extends AppCompatActivity {

    private PostViewModel postViewModel;
    private Post post;
    private TextInputEditText contentEditText;
    private TextInputEditText contactEditText;
    private CheckBox availableCheckBox;
    private ImageView postImg;
    private ProgressBar progressBar;
    private Bitmap postImgBitmap;
    private final int REQUEST_CODE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_post);
        postViewModel = new ViewModelProvider(this).get(PostViewModel.class);
        post = new Gson().fromJson(getIntent().getExtras().getString("post"), Post.class);
        contentEditText = findViewById(R.id.edit_post_content_edit_text);
        contactEditText = findViewById(R.id.edit_post_contact_edit_text);
        availableCheckBox = findViewById(R.id.edit_post_available);
        postImg = findViewById(R.id.edit_post_image_view);
        progressBar = findViewById(R.id.edit_post_progress_bar);
        contentEditText.setText(post.getPostContent());
        contactEditText.setText(post.getContact());
        Picasso.get().load(post.getPostImgUrl()).noPlaceholder().into(postImg);
        availableCheckBox.setChecked(post.isAvailable());
        progressBar.setVisibility(View.INVISIBLE);
    }

    public void updatePost(View view) {
        progressBar.setVisibility(View.VISIBLE);
        post.setPostContent(contentEditText.getText().toString());
        post.setContact(contactEditText.getText().toString());
        post.setAvailable(availableCheckBox.isChecked());
        post.setLastUpdated(Timestamp.now());
        if (postImgBitmap != null)
            StoreModel.uploadImage(postImgBitmap, new StoreModel.Listener() {
                @Override
                public void onSuccess(String url) {
                    post.setPostImgUrl(url);
                    postViewModel.update(post, data -> {
                        if (data) {
                            Toast.makeText(EditPostActivity.this, "Post updated successfully", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(EditPostActivity.this, "Couldn't update post", Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                    });
                }

                @Override
                public void onFail() {
                    progressBar.setVisibility(View.INVISIBLE);
                    Snackbar.make(view, "Failed to create post and save it in databases", Snackbar.LENGTH_LONG).show();
                }
            });

        else
            postViewModel.update(post, data -> {
                if (data) {
                    Toast.makeText(EditPostActivity.this, "Post updated successfully", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(EditPostActivity.this, "Couldn't update post", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
    }

    public void chooseImageFromGallery(View view) {
        try {
            Intent openGalleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
            openGalleryIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");

            startActivityForResult(openGalleryIntent, REQUEST_CODE);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null) return;
        if (data.getData() != null && resultCode == RESULT_OK) {
            Uri postImgUri = data.getData();
            postImg.setImageURI(postImgUri);
            postImgBitmap = uriToBitmap(postImgUri);
        } else {
            Toast.makeText(getApplicationContext(), "No image was selected", Toast.LENGTH_SHORT).show();
        }
    }

    private Bitmap uriToBitmap(Uri selectedFileUri) {
        try {
            ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(selectedFileUri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
            parcelFileDescriptor.close();
            return image;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void removePost(View view) {
        if (post == null) return;
        new AlertDialog.Builder(this).setTitle("Are you sure you want to delete the post?").setPositiveButton("Yes", (dialogInterface, i) -> postViewModel.delete(post, data -> {
            if (data) {
                Toast.makeText(getApplicationContext(), "Post deleted successfully", Toast.LENGTH_SHORT).show();
                finish();
            } else
                Toast.makeText(getApplicationContext(), "Couldn't delete post", Toast.LENGTH_SHORT).show();
        })).setNegativeButton("No", (dialogInterface, i) -> dialogInterface.dismiss()).show();

    }
}