package com.fsd.mineher.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.SearchView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.fsd.mineher.R;
import com.fsd.mineher.adapter.UserRecyclerAdapter;
import com.fsd.mineher.model.ModelFirebase;
import com.fsd.mineher.model.User;

import java.util.ArrayList;
import java.util.List;

public class FindUsersActivity extends AppCompatActivity {

    private RecyclerView userRecycler;
    private UserRecyclerAdapter adapter;
    private android.widget.SearchView searchView;
    private List<User> users = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_users);
        userRecycler = findViewById(R.id.find_users_recycler_view);
        searchView = findViewById(R.id.find_users_search_view);
        userRecycler.setHasFixedSize(true);
        adapter = new UserRecyclerAdapter();
        userRecycler.setAdapter(adapter);
        adapter.setOnItemClickListener(position -> {
            Intent intent = new Intent(getApplicationContext(), OtherUserActivity.class);
            intent.putExtra("otherUser", adapter.getUsers().get(position));
            startActivity(intent);
        });
        ModelFirebase.getAllOtherUsers(data -> {
            users = data;
            adapter.setUsers(users);
        });


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                List<User> filteredUsers = new ArrayList<>();
                if (s.isEmpty())
                    filteredUsers.addAll(users);
                else
                    for (User user : users)
                        if (user.getUserUsername().contains(s))
                            filteredUsers.add(user);
                adapter.setUsers(filteredUsers);
                return false;
            }
        });
    }
}