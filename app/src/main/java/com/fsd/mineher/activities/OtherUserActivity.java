package com.fsd.mineher.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.fsd.mineher.R;
import com.fsd.mineher.model.ModelFirebase;
import com.fsd.mineher.model.User;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class OtherUserActivity extends AppCompatActivity {
    private User otherUser;
    private TextView otherUserUsername;
    private TextView otherUserEmail;
    private TextView otherUserInfo;
    private CircleImageView otherUserProfileImage;
    private Button followBtn;
    private Button unfollowBtn;
    private Button otherUserPosts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_user);
        otherUser = (User) getIntent().getExtras().get("otherUser");

        otherUserUsername = findViewById(R.id.other_user_username_text_view);
        otherUserEmail = findViewById(R.id.other_user_email_text_view);
        otherUserInfo = findViewById(R.id.other_user_info_text);
        otherUserProfileImage = findViewById(R.id.other_user_profile_image_view);
        unfollowBtn = findViewById(R.id.other_user_unfollow_btn);
        followBtn = findViewById(R.id.other_user_follow_btn);
        if (User.getInstance().getFollowingUsers().contains(otherUser.getUserId())) {
            followBtn.setEnabled(false);
            unfollowBtn.setEnabled(true);
        }
        otherUserPosts = findViewById(R.id.other_user_posts_btn);

        otherUserUsername.setText(otherUser.getUserUsername());
        otherUserEmail.setText(otherUser.getUserEmail());
        otherUserInfo.setText(otherUser.getUserInfo());
        Picasso.get().load(otherUser.getProfileImageUrl()).noPlaceholder().into(otherUserProfileImage);

        otherUserPosts.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), PostsActivity.class);
            intent.putExtra("user", otherUser);
            startActivity(intent);
        });

    }

    public void followUser(View view) {
        User.getInstance().getFollowingUsers().add(otherUser.getUserId());
        followBtn.setEnabled(false);
        unfollowBtn.setEnabled(true);
        ModelFirebase.updateUser("followingUsers", User.getInstance().getFollowingUsers());
    }

    public void unfollowUser(View view) {
        User.getInstance().getFollowingUsers().remove(otherUser.getUserId());
        unfollowBtn.setEnabled(false);
        followBtn.setEnabled(true);
        ModelFirebase.updateUser("followingUsers", User.getInstance().getFollowingUsers());
    }
}