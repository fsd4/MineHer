package com.fsd.mineher;

import android.app.Activity;
import android.content.Intent;
import android.provider.MediaStore;
import android.widget.ImageView;
import android.widget.Toast;
//import androidx.multidex.MultiDexApplication;

import com.fsd.mineher.MineHerApplication;

public class Utils
{
    static int REQUEST_CODE = 1;

    public static void chooseImageFromGallery(Activity sender)
    {

        try
        {
            Intent openGalleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
            openGalleryIntent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");

            sender.startActivityForResult(openGalleryIntent, REQUEST_CODE);
        }
        catch (Exception e)
        {
            Toast.makeText(MineHerApplication.getAppContext(),  e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
