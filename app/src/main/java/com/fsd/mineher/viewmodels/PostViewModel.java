package com.fsd.mineher.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.fsd.mineher.model.Model;
import com.fsd.mineher.model.ModelFirebase;
import com.fsd.mineher.model.Post;
import com.fsd.mineher.repositories.PostRepo;

import java.util.List;

public class PostViewModel extends AndroidViewModel {
    private final PostRepo repo;

    public PostViewModel(@NonNull Application application) {
        super(application);
        repo = new PostRepo(application);
    }

    public void insert(Post post, Model.Listener<Boolean> listener) {
        repo.insert(post);
        ModelFirebase.addPost(post, listener);
    }
    public void update(Post post, Model.Listener<Boolean> listener){
        repo.update(post);
        ModelFirebase.updatePost(post, listener);
    }

    public void delete(Post post, Model.Listener<Boolean> listener){
        repo.delete(post);
        ModelFirebase.deletePost(post, listener);
    }
    public void deleteAllPosts(){
        repo.deleteAllPosts();
    }

    public void insertLocaly(Post post) {
        repo.insert(post);
    }

    public void delete(Post post) {
        repo.delete(post);
    }

    public LiveData<List<Post>> getAllPosts() {
        return repo.getAllPosts();
    }

    public LiveData<List<Post>> getAllPostsById(String userId) {
        return repo.getAllPostsById(userId);
    }


}
