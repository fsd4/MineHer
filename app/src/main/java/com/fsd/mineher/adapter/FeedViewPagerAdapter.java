package com.fsd.mineher.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.fsd.mineher.fragments.FollowingFragment;
import com.fsd.mineher.fragments.NearbyFragment;

public class FeedViewPagerAdapter extends FragmentStateAdapter {

    public FeedViewPagerAdapter(@NonNull Fragment fragment) {
        super(fragment);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0: {
                return new FollowingFragment();
            }
            default: {
                return new NearbyFragment();
            }
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}