package com.fsd.mineher.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.fsd.mineher.R;
import com.fsd.mineher.interfaces.OnPostItemListener;
import com.fsd.mineher.model.ModelFirebase;
import com.fsd.mineher.model.Post;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PostRecyclerAdapter extends RecyclerView.Adapter<PostRecyclerAdapter.PostHolder> {
    private List<Post> posts = new ArrayList<>();
    private OnPostItemListener onItemClickListener;

    public void setOnPostClickListener(OnPostItemListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
        notifyDataSetChanged();
    }

    public List<Post> getPosts() {
        return posts;
    }

    @NonNull
    @Override
    public PostHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.post_item, parent, false);
        return new PostHolder(itemView, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull PostHolder holder, int position) {
        Post post = posts.get(position);
        ModelFirebase.getUser(post.getUserId(), data -> {
            if (!post.getUsername().contentEquals(data.getUserUsername()))
                posts.get(position).setUsername(data.getUserUsername());
            holder.username.setText(posts.get(position).getUsername());

            if (!post.getUserProfileImageUrl().contentEquals(data.getProfileImageUrl()))
                posts.get(position).setUserProfileImageUrl(data.getProfileImageUrl());
            Picasso.get().load(post.getUserProfileImageUrl()).noPlaceholder().into(holder.profileImg);
        });

        if (post.isAvailable()) {
            holder.isAvailable.setText("Available");
            holder.isAvailable.setTextColor(holder.itemView.getResources().getColor(R.color.green));
        } else {
            holder.isAvailable.setText("Sold");
            holder.isAvailable.setTextColor(holder.itemView.getResources().getColor(R.color.red));
        }
        Picasso.get().load(post.getPostImgUrl()).noPlaceholder().into(holder.postImg);
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    class PostHolder extends RecyclerView.ViewHolder {
        private final CircleImageView profileImg;
        private final TextView username;
        private final ImageView postImg;
        private final TextView isAvailable;

        public PostHolder(@NonNull View itemView, OnPostItemListener onItemClickListener) {
            super(itemView);
            profileImg = itemView.findViewById(R.id.post_profile_img);
            username = itemView.findViewById(R.id.post_username);
            postImg = itemView.findViewById(R.id.post_img);
            isAvailable = itemView.findViewById(R.id.post_availability);
            username.setOnClickListener(v -> {
                if (onItemClickListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION)
                        onItemClickListener.onUserNameClick(v, position);
                }
            });
            itemView.setOnClickListener(v -> {
                if (onItemClickListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION)
                        onItemClickListener.onItemClick(v, position);
                }
            });
        }
    }
}
